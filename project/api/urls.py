"""API URL."""

from django.conf.urls import url

from api.endpoints.auth_endpoint import (
    obtain_request_token,
    refresh_request_token,
    verify_request_token
)

urlpatterns = [
    url(r'get-reques-token/', obtain_request_token),
    url(r'refresh-reques-token/', refresh_request_token),
    url(r'verify-reques-token/', verify_request_token),
]
