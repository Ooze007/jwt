"""Auth API endpoint."""

from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response

from rest_framework_jwt.compat import get_request_data

from api.settings import api_request_settings
from api.serializers.request_token import (
    RequestTokenSerializer,
    VerifyRequestTokenSerializer,
    RefreshRequestTokenSerializer
)

jwt_response_payload_handler = (
    api_request_settings.JWT_RESPONSE_PAYLOAD_HANDLER
)


class JSONWebTokenAPIView(APIView):
    """Base API View that various JWT interactions inherit from."""

    permission_classes = ()
    authentication_classes = ()

    def get_serializer_context(self):
        """Extra context provided to the serializer class."""
        return {
            'request': self.request,
            'view': self,
        }

    def get_serializer_class(self):
        """
            Return the class to use for the serializer.

        Defaults to using `self.serializer_class`.
        You may want to override this if you need to provide different
        serializations depending on the incoming request.
        (Eg. admins get full serialization, others get basic serialization)
        """
        assert self.serializer_class is not None, (
            "'%s' should either include a `serializer_class` attribute, "
            "or override the `get_serializer_class()` method."
            % self.__class__.__name__)
        return self.serializer_class

    def get_serializer(self, *args, **kwargs):
        """
            Return the serializer instance.

        Return the serializer instance that should be used for validating and
        deserializing input, and for serializing output.
        """
        serializer_class = self.get_serializer_class()
        kwargs['context'] = self.get_serializer_context()

        return serializer_class(*args, **kwargs)

    def post(self, request):
        """POST."""
        serializer = self.get_serializer(
            data=get_request_data(request)
        )

        if serializer.is_valid():
            user = serializer.object.get('user') or request.user
            token = serializer.object.get('token')
            response_data = jwt_response_payload_handler(token, user, request)

            return Response(response_data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ObtainRequestToken(JSONWebTokenAPIView):
    """
        API View that receives a POST with a user's username and password.

    Returns a JSON Web Token that can be used for authenticated requests.
    """

    serializer_class = RequestTokenSerializer


class VerifyRequestToken(JSONWebTokenAPIView):
    """
        Verify request token.

    API View that checks the veracity of a token, returning the token if it
    is valid.
    """

    serializer_class = VerifyRequestTokenSerializer


class RefreshRequestToken(JSONWebTokenAPIView):
    """
        Refresh request token.

    API View that returns a refreshed token (with new expiration) based on
    existing token.

    If 'orig_iat' field (original issued-at-time) is found, will first check
    if it's within expiration window, then copy it to the new token
    """

    serializer_class = RefreshRequestTokenSerializer


obtain_request_token = ObtainRequestToken.as_view()
refresh_request_token = VerifyRequestToken.as_view()
verify_request_token = RefreshRequestToken.as_view()
