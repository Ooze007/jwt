"""Api settings."""

from django.conf import settings
from rest_framework.settings import APISettings

from rest_framework_jwt.settings import DEFAULTS, IMPORT_STRINGS

USER_REQUEST_SETTINGS = getattr(settings, 'JWT_AUTH_REQUEST', None)
USER_ACCESS_SETTINGS = getattr(settings, 'JWT_AUTH', None)

api_request_settings = APISettings(USER_REQUEST_SETTINGS,
                                   DEFAULTS, IMPORT_STRINGS)

api_access_settings = APISettings(USER_ACCESS_SETTINGS,
                                  DEFAULTS, IMPORT_STRINGS)
