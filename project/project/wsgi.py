"""
WSGI config for project project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

activate_this = u'/home/ooze/projects/jwt/venv/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

import os
import sys

project_home = u'/usr/home/ooze/projects/jwt/project'
if project_home not in sys.path:
    sys.path.append(project_home)

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "project.settings")

application = get_wsgi_application()
